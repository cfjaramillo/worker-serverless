using worker.serverless.entities;
namespace worker.serverless.business.interfaces;

public interface IWorkerBusiness 
{
    Task<IEnumerable<UserModel>> GetUsersAsync();
    Task<IEnumerable<UserModel>> GetUsersByIdAsync(string userId);
}