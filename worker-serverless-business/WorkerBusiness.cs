using worker.serverless.entities;
using worker.serverless.business.interfaces;
namespace worker.serverless.business;

public class WorkerBusiness : IWorkerBusiness
{
    private readonly DynamoDbContext _userDbContext;
    private readonly APIGatewayRequestModel _request;

    public WorkerBusiness(DynamoDbContext userDbContext, APIGatewayRequestModel request)
    {
        this._userDbContext = userDbContext;
        this._request = request;
    }

    public async Task<IEnumerable<UserModel>> GetUsersAsync()
    {
        try
        {
            var result = await _userDbContext.GetUsersAsync();
            return result;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public async Task<IEnumerable<UserModel>> GetUsersByIdAsync(string userId)
    {
        try
        {
            var result = await _userDbContext.GetUsersByIdAsync(userId);
            return result;
        }
        catch (Exception)
        {
            throw;
        }
    }



}