namespace worker.serverless.entities;

public class APIGatewayRequestModel
{
    public String Resource { get; set; }
    public String Body { get; set; }
    public Object RequestContext { get; set; }
    public String HttpMethod { get; set; }
    public Object Headers { get; set; }
    public Dictionary<String,String> PathParameters { get; set; }
    public Object QueryStringParameters { get; set; }

}