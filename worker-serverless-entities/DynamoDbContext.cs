namespace worker.serverless.entities;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;

public class DynamoDbContext
{
    private readonly IAmazonDynamoDB _dynamoDb;

    public DynamoDbContext(IAmazonDynamoDB dynamoDb)
    {
        this._dynamoDb = dynamoDb;
    }

    public async Task<IEnumerable<UserModel>> GetUsersAsync()
    {
        var userList = new List<UserModel>();
        var result = await _dynamoDb.ScanAsync(new ScanRequest { TableName = "workerUser" });

        if (result != null)
        {
            foreach (var item in result.Items)
            {
                item.TryGetValue("UserId", out var Id);
                item.TryGetValue("Name", out var Name);
                item.TryGetValue("Phone", out var Phone);
                item.TryGetValue("Email", out var Email);
                item.TryGetValue("Location", out var Location);

                userList.Add(new UserModel()
                {
                    Id = Id?.S,
                    Name = Name?.S,
                    Phone = Phone?.S,
                    Email = Email?.S,
                    Location = Location?.S
                });
            }
        }

        return userList;
    }

    public async Task<IEnumerable<UserModel>> GetUsersByIdAsync(string userId)
    {
        var userList = new List<UserModel>();
        var result = await _dynamoDb.ScanAsync(new ScanRequest
        {
            TableName = "workerUser",
            ExpressionAttributeValues = new Dictionary<string, AttributeValue> {
                {":UserId", new AttributeValue { S = userId }}
            },
            FilterExpression = "UserId = :UserId"
        });

        if (result != null)
        {
            foreach (var item in result.Items)
            {
                item.TryGetValue("UserId", out var Id);
                item.TryGetValue("Name", out var Name);
                item.TryGetValue("Phone", out var Phone);
                item.TryGetValue("Email", out var Email);
                item.TryGetValue("Location", out var Location);

                userList.Add(new UserModel()
                {
                    Id = Id?.S,
                    Name = Name?.S,
                    Phone = Phone?.S,
                    Email = Email?.S,
                    Location = Location?.S
                });
            }
        }

        return userList;
    }




}