using Xunit;
using Amazon.Lambda.Core;
using Amazon.Lambda.TestUtilities;
using Amazon.Lambda.APIGatewayEvents;
using System.Net;
using worker.serverless.business;

namespace worker_userdb_operations_function.Tests;

public class FunctionTest
{
    [Fact]
    public void FunctionHandler_Success()
    {
        /// Arrange
        var request = new APIGatewayProxyRequest();
        var function = new Function();
        var context = new TestLambdaContext();
        /// Act
        var result = function.FunctionHandler(request, context);
        /// Assert
        Assert.NotNull(result);
        Assert.Equal((int)HttpStatusCode.OK, result.Result.StatusCode);
    }
}
