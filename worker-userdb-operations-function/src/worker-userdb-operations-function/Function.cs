using Amazon.Lambda.Core;
using Amazon.Lambda.APIGatewayEvents;
using System.Text.Json;
using System.Net;
using worker.serverless.entities;
using Amazon.DynamoDBv2;
using worker.serverless.business;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace worker_userdb_operations_function;

public class Function
{

    /// <summary>
    /// Function that handles DynamoDB Operations
    /// </summary>
    /// <param name="input"></param>
    /// <param name="context"></param>
    /// <returns></returns>
    public async Task<APIGatewayProxyResponse> FunctionHandler(APIGatewayProxyRequest input, ILambdaContext context)
    {
        try
        {

            Object response = null;
            WorkerBusiness business = null;
            DynamoDbContext _context = null;
            var request = JsonSerializer.Serialize(input);
            APIGatewayRequestModel result = JsonSerializer.Deserialize<APIGatewayRequestModel>(request);

            if (result.HttpMethod != null)
            {
                _context = new DynamoDbContext(new AmazonDynamoDBClient());
                business = new WorkerBusiness(_context, result);
            }

            switch (result.Resource)
            {
                case "/user":
                    response = await business.GetUsersAsync();
                    break;
                case "/user/{userid}":
                    String id = String.Empty;
                    if (result.PathParameters.TryGetValue("userid", out id))
                    {
                        response = await business.GetUsersByIdAsync(id);
                    }
                    break;
                default:
                    response = "default";
                    break;
            }


            return new APIGatewayProxyResponse
            {
                Body = JsonSerializer.Serialize(response),
                StatusCode = (int)HttpStatusCode.OK
            };
        }
        catch (System.Exception ex)
        {
            return new APIGatewayProxyResponse
            {
                Body = JsonSerializer.Serialize(ex.Message),
                StatusCode = (int)HttpStatusCode.BadRequest
            };
        }

    }
}
